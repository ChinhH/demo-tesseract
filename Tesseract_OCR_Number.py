import cv2
import os
import pytesseract
import cv2
import numpy as np
import urllib
from PIL import Image
pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files/Tesseract-OCR/tesseract'
PATH_TEST='D:/_ChinhH_/ocr_number_metarial/test'
filenames_test=[]
filenames_test = [f for f in os.listdir(PATH_TEST) if f.endswith(".jpg") or f.endswith(".JPG")]
for file in filenames_test:
	nn = np.fromfile(PATH_TEST+'/'+file, np.uint8)
	img = cv2.imdecode(nn, cv2.IMREAD_COLOR)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	cv2.imshow('',img)
	cv2.waitKey(0)
	imcrp = img[3:int(img.shape[0])-3,3:int(img.shape[1])-3]
	ret, img = cv2.threshold(imcrp,100,200,1)
	ocr_result = pytesseract.image_to_string(img, lang='eng', boxes=False, \
           config='--psm 10 --oem 3 -c tessedit_char_whitelist=0123456789')
	print('output:',ocr_result)
	cv2.imshow('',img)
	cv2.waitKey(0)